<!--TITLE -->
<div class="row">
    <div id="paper-top">
        <div class="col-sm-12">
            <h2 class="tittle-content-header">
                <span class="entypo-info-circled"></span> 
                <span>Welcome
                </span>
            </h2>

        </div>

        
    </div>
</div>
<!--/ TITLE -->

<div class="content-wrap">
    <div class="row">
        <div class="col-sm-12">

            <div class="nest" id="notifClose">
                <div class="title-alt">
                    <div class="titleClose">
                        <a class="gone" href="#notifClose">
                            <span class="entypo-cancel"></span>
                        </a>
                    </div>
                    <div class="titleToggle">
                        <a class="nav-toggle-alt" href="#notif">
                            <span class="entypo-up-open"></span>
                        </a>
                    </div>

                </div>

                <div id="notif" style="display:block" class="body-nest">
                	<div class="alert alert-success">
					    <button data-dismiss="alert" class="close" type="button">×</button>
					    <span class="entypo-thumbs-up"></span>
					    <strong>Hi, </strong>&nbsp;&nbsp;Selamat Datang kembali <strong><?php echo $_SESSION['nama']; ?></strong>, status anda sebagai <strong><small><i><?php echo $_SESSION['level']; ?></i></small></strong>
					</div>
                </div>
            </div>
        </div>


        <?php 
        if ($_SESSION['level'] != "Karyawan" AND $_SESSION['level'] != "Administrasi IT") {
            ?>

        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading">Status Server</div>
                <div class="panel-body">
                    <?php
                        $url = "https://server-status-tsp.firebaseapp.com/status";
                        $baca = file_get_contents($url);
                        $data = json_decode($baca, true);
                    ?>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>ID</td>
                                <td>Alias</td>
                                <td>Location</td>
                                <td>Uptime</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php   
                                $no = 1;
                                for($a=0; $a < count($data); $a++)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $no ++; ?></td>
                                        <td><?php echo $data[$a]['id']; ?></td>
                                        <td><?php echo $data[$a]['alias']; ?></td>
                                        <td><?php echo $data[$a]['location']; ?></td>
                                        <td><?php echo $data[$a]['uptime']; ?></td>
                                        <td>
                                            <?php
                                                if ($data[$a]['status'] != "UP") {
                                                    ?>
                                                    <button type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></button>
                                                    <?php
                                                }else{
                                                    ?>
                                                    <button type="button" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></button>
                                                    <?php
                                                }
                                            ?>
                                            
                                        
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                </div>
            </div>
        </div>
                <?php
            }
        ?>
    </div>
</div>