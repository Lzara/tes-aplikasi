<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Kerusakan dan Perbaikan.xls");
?>
<h3>Laporan Kerusakan dan Perbaikan</h3>
<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Karyawan</th>
			<th>Keterangan</th>
			<th>Tgl Pengajuan</th>
			<th>Tgl Perbaikan</th>
			<th>Tgl Selesai</th>
			<th>Status</th>
			<th>Petugas</th>
		</tr>
	</thead>
	<tbody>
		<?php
		include 'koneksi.php';
		$awal = date("Y-m-d", strtotime($_POST['awal']));
		$akhir = date("Y-m-d", strtotime($_POST['akhir']));
		$sql = mysql_query("SELECT * FROM pengaduan WHERE tanggalPengajuan BETWEEN '$awal' AND '$akhir' ORDER BY tanggalPengajuan DESC");
		$no = 1;

		while ($data = mysql_fetch_array($sql)) {
			?>
			<tr>
				<td><?php echo $no ++; ?></td>
				<td><?php echo $data['namaPengajuan']; ?></td>
				<td><?php echo $data['keterangan']; ?></td>
				<td><?php echo $data['tanggalPengajuan']; ?></td>
				<td><?php echo $data['tanggalProses']; ?></td>
				<td><?php echo $data['tanggalSelesai']; ?></td>
				<td><?php echo $data['status']; ?></td>
				<td><?php echo $data['petugas']; ?></td>
			</tr>
			<?php
		}
		?>
	</tbody>
</table>
</body>
</html>