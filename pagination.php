<?php
function paginate_one($reload, $pag, $tpags) {
	
	$firstlabel = "&laquo;&nbsp;";
	$prevlabel  = "&lsaquo;&nbsp;";
	$nextlabel  = "&nbsp;&rsaquo;";
	$lastlabel  = "&nbsp;&raquo;";
	
	$out = "<ul class=\"pagination\">";
	
	// first
	if($pag>1) {
		$out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>";
	}
	else {
		$out.= "<li><span>" . $firstlabel . "</span></li>";
	}
	
	// previous
	if($pag==1) {
		$out.= "<li><span>" . $prevlabel . "</span></li>";
	}
	elseif($pag==2) {
		$out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>";
	}
	else {
		$out.= "<li><a href=\"" . $reload . "&amp;pag=" . ($pag-1) . "\">" . $prevlabel . "</a></li>";
	}
	
	// current
	$out.= "<li><span class=\"current\">page " . $pag . " of " . $tpags ."</span></li>";
	
	// next
	if($pag<$tpags) {
		$out.= "<li><a href=\"" . $reload . "&amp;pag=" .($pag+1) . "\">" . $nextlabel . "</a></li>";
	}
	else {
		$out.= "<li><span>" . $nextlabel . "</span></li>";
	}
	
	// last
	if($pag<$tpags) {
		$out.= "<li><a href=\"" . $reload . "&amp;pag=" . $tpags . "\">" . $lastlabel . "</a></li>";
	}
	else {
		$out.= "<li><span>" . $lastlabel . "</span></li>";
	}
	
	$out.= "</ul>";
	
	return $out;
}
?>