-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 22, 2018 at 03:13 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id` int(11) NOT NULL,
  `namaPengajuan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggalPengajuan` date NOT NULL,
  `tanggalProses` date NOT NULL,
  `tanggalSelesai` date NOT NULL,
  `status` enum('Menunggu Antrian','Proses Perbaikan','Selesai Diperbaiki','') NOT NULL,
  `petugas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengaduan`
--

INSERT INTO `pengaduan` (`id`, `namaPengajuan`, `keterangan`, `tanggalPengajuan`, `tanggalProses`, `tanggalSelesai`, `status`, `petugas`) VALUES
(1, 'Jono', 'Kabel Jaringan Putus', '2018-10-19', '0000-00-00', '0000-00-00', 'Selesai Diperbaiki', 'Lutfiedadi'),
(2, 'dedi', 'kdasdsa', '1970-01-01', '2018-10-20', '2018-10-20', 'Selesai Diperbaiki', 'Lutfiedadi'),
(3, 'dedi', 'kunci', '2018-10-20', '2018-10-20', '0000-00-00', 'Proses Perbaikan', 'Lutfiedadi'),
(4, 'dedi', 'lemari', '2018-10-20', '2018-10-20', '0000-00-00', 'Proses Perbaikan', 'Lutfiedadi'),
(5, 'dedi', 'dispenser', '2018-10-22', '2018-10-22', '0000-00-00', 'Proses Perbaikan', 'Sucipto'),
(6, 'dedi', 'meja lipat', '2018-10-24', '2018-10-22', '0000-00-00', 'Proses Perbaikan', 'Sugandi');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_anggota` int(3) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(1000) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` enum('Karyawan','IT Suport','Manager IT','Administrasi IT') NOT NULL,
  `status` enum('aktif','nonaktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_anggota`, `nama`, `username`, `password`, `level`, `status`) VALUES
(1, 'Lutfiedadi', 'lutfie', '202cb962ac59075b964b07152d234b70', 'IT Suport', 'aktif'),
(2, 'Dedi', 'dedi', 'caf1a3dfb505ffed0d024130f58c5cfa', 'Karyawan', 'aktif'),
(3, 'Adi', 'Adi', '698d51a19d8a121ce581499d7b701668', 'Manager IT', 'aktif'),
(4, 'Sucipto', 'cipto', '202cb962ac59075b964b07152d234b70', 'IT Suport', 'aktif'),
(5, 'Sugandi', 'gandi', '202cb962ac59075b964b07152d234b70', 'IT Suport', 'aktif'),
(6, 'Julfa', 'julfa', 'caf1a3dfb505ffed0d024130f58c5cfa', 'Karyawan', 'aktif'),
(7, 'Susi', 'susi', 'bcbe3365e6ac95ea2c0343a2395834dd', 'Administrasi IT', 'aktif');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_anggota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_anggota` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
