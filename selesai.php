<div class="panel panel-success">
						<div class="panel-heading">Daftar Selesai Perbaikan</div>
						<div class="panel-body">

						<?php

                	
							//        mengatur variabel reload dan sql
						    if(isset($_REQUEST['sls']) && $_REQUEST['sls']<>""){
							//        jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)
							//        pakai ini
						        $sls=$_REQUEST['sls'];

						        $reload = "home.php?page=pengaduan&pagination=true&sls=$sls";
						        $sql =  "SELECT * FROM pengaduan WHERE status = 'Selesai Diperbaiki' and namaPengajuan LIKE '%$sls%' ORDER BY id DESC";
						        $result = mysql_query($sql);
						    }else{
							//            jika tidak ada pencarian pakai ini				        	
						        $reload = "home.php?page=pengaduan&pagination=true";
						        $sql =  "SELECT * FROM pengaduan WHERE status = 'Selesai Diperbaiki' ORDER BY id DESC";
						        $result = mysql_query($sql);
						    }
						    
						    //pagination config start
						    $rpp = 5; // jumlah record per halaman
						    $pag = intval($_GET["pag"]);
						    if($pag<=0) $pag = 1;  
						    $tcount = mysql_num_rows($result);
						    $tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
						    $count = 0;
						    $i = ($pag-1)*$rpp;
						    $no_urut = ($pag-1)*$rpp;
						    //pagination config end
						?>
						<div class="row">
						    <div class="col-lg-2">
						        <!--muncul jika ada pencarian (tombol reset pencarian)-->
						        <?php
						        if($_REQUEST['sls']<>""){
						        ?>
						            <a class="btn btn-default btn-outline" href="home.php?page=pengaduan"> Reset Pencarian</a>
						        <?php
						        }
						        ?>
						    </div>
						    <div class="col-lg-10 text-right">
						        <form role="form" class="form-inline" method="post" action="home.php?page=pengaduan">
								    <div class="form-group">
								        <input type="text" name="sls" class="form-control" placeholder="Search..." value="<?php echo $_REQUEST['sls']; ?>">
								    </div>
								    <button class="btn btn-primary" type="submit">Cari</button>
								</form>
						    </div>
						</div><br>
						<table class="table table-bordered">
						    <thead>
						        <tr>
						            <th>#</th>
									<th>Nama Pengajuan</th>
									<th>Keterangan</th>
									<th>Tgl Pengajuan</th>
									<th>Tgl Proses</th>
									<th>Tgl Selesai</th>
									<th>Status</th>
									<th>Aksi</th>
						        </tr>
						    </thead>
						    <tbody>
						        <?php
						        while(($count<$rpp) && ($i<$tcount)) {
						            mysql_data_seek($result,$i);
						            $data = mysql_fetch_array($result);
						        ?>
						        <tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $data['namaPengajuan']; ?></td>
									<td><?php echo $data['keterangan']; ?></td>
									<td><?php echo date("d-m-Y", strtotime($data['tanggalPengajuan'])); ?></td>
									<td><?php echo date("d-m-Y", strtotime($data['tanggalProses'])); ?></td>
									<td><?php echo date("d-m-Y", strtotime($data['tanggalSelesai'])); ?></td>
									<td><?php echo $data['status']; ?></td>
									<td></td>
								</tr>
						        <?php
						            $i++; 
						            $count++;
						        }
						        ?>
						    </tbody>
						</table>
						<div><?php echo paginate_one($reload, $pag, $tpages); ?></div>
					</div>
					</div>