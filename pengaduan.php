<?php 
    if ($_SESSION['level'] == 'Karyawan') {
        ?>
		<div class="col-lg-4">
			<form action="#" method="POST">
			<div class="form-group">
			  <label for="usr">Tanggal Pengajuan</label>
			  <input type="hidden" name="namaPengajuan" value="<?php echo $_SESSION['nama']; ?>" class="form-control" >
			  <input type="date" name="tanggalPengajuan" class="form-control" >
			</div>
			<div class="form-group">
			  <label for="usr">Pengaduan Kerusakan</label>
			  <input type="text" name="keterangan" class="form-control" >
			</div>
			<div class="form-group">
			  <input type="submit" name="simpan" value="Simpan" class="btn btn-info" >
			</div>
			</form>
		</div>
		<div class="col-lg-8">
			<div class="panel panel-success">
						<div class="panel-heading">Daftar Pengaduan Kerusakan</div>
						<div class="panel-body">
			
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Pengajuan</th>
								<th>Keterangan</th>
								<th>Tgl Pengajuan</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$nama = $_SESSION['nama'];
						$no = 1;
						$sql = mysql_query("SELECT * FROM pengaduan WHERE namaPengajuan = '$nama' ORDER BY ID DESC");
						while ($data = mysql_fetch_array($sql)) {
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $data['namaPengajuan']; ?></td>
								<td><?php echo $data['keterangan']; ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalPengajuan'])); ?></td>
								<td>
									<?php
									if ($data['status'] == "Menunggu Antrian") {
										?>
											<button type="button" class="btn btn-danger"><?php echo $data['status']; ?></button>
										<?php
									}elseif ($data['status'] == "Proses Perbaikan") {
										?>
											<button type="button" class="btn btn-info"><?php echo $data['status']; ?></button>
										<?php
									}else{
										?>
											<button type="button" class="btn btn-success"><?php echo $data['status']; ?></button>
										<?php
									}
									?>
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
					</div>
		</div>
	<?php
    }
?>

<?php 
    if ($_SESSION['level'] == "IT Suport") {
        ?>
        <div class="col-lg-12">
        	<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#Antrian" aria-controls="Antrian" role="tab" data-toggle="tab">Antrian</a></li>
				<li role="presentation"><a href="#Proses" aria-controls="Proses" role="tab" data-toggle="tab">Proses</a></li>
				<li role="presentation"><a href="#Selesai" aria-controls="Selesai" role="tab" data-toggle="tab">Selesai</a></li>
			</ul><br>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="Antrian">
					<div class="panel panel-danger">
						<div class="panel-heading">Daftar Antrian Pengaduan</div>
						<div class="panel-body">
			
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Pengajuan</th>
								<th>Keterangan</th>
								<th>Tgl Pengajuan</th>
								<th>Tgl Proses</th>
								<th>Petugas</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$sql = mysql_query("SELECT * FROM pengaduan WHERE status = 'Menunggu Antrian'");
						while ($data = mysql_fetch_array($sql)) {
							?>
							<form action="#" method="POST">
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $data['namaPengajuan']; ?></td>
								<td><?php echo $data['keterangan']; ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalPengajuan'])); ?></td>
								<td>
									<?php
									if ($data['tanggalProses'] = "0000-00-00") {
									 	?>
									 	<input type="date" name="tanggalProses" class="form-control">
									 	<input type="hidden" name="id" value="<?php echo $data['id'] ?>" class="form-control">
									 	<?php
									}elseif ($data['tanggalProses'] != '0000-00-00') {
										echo date("d-m-Y", strtotime($data['tanggalProses']));
									} 
									?>
								</td>
								<td>
									<select name="petugas" class="form-control">
										<option></option>
										<?php
										$sql = mysql_query("SELECT * FROM user WHERE level = 'IT Suport' ORDER BY nama ASC");
										while ($data = mysql_fetch_array($sql)) {
											?>
											<option value="<?php echo $data['nama'] ?>"><?php echo $data['nama'] ?></option>
											<?php
										}
										?>
									</select>									
								</td>
								<td><input type="submit" value="Proses" class="btn btn-success" name="proses"></td>
							</tr>
							</form>
							<?php
						}
						?>
						</tbody>
					</table>
					</div>
					</div>
				</div>


				<div role="tabpanel" class="tab-pane" id="Proses">
					<div class="panel panel-info">
						<div class="panel-heading">Daftar Proses Perbaikan</div>
						<div class="panel-body">
			
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Pengajuan</th>
								<th>Keterangan</th>
								<th>Tgl Pengajuan</th>
								<th>Tgl Selesai</th>
								<th>Petugas</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$sql = mysql_query("SELECT * FROM pengaduan WHERE status = 'Proses Perbaikan'");
						while ($data = mysql_fetch_array($sql)) {
							?>
							<form action="#" method="POST">
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $data['namaPengajuan']; ?></td>
								<td><?php echo $data['keterangan']; ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalPengajuan'])); ?></td>
								<td>
									<?php
									if ($data['tanggalSelesai'] = "0000-00-00") {
									 	?>
									 	<input type="date" name="tanggalSelesai" class="form-control">
									 	<input type="hidden" name="id" value="<?php echo $data['id'] ?>" class="form-control">
									 	<input type="hidden" name="petugas" value="<?php echo $_SESSION['nama'] ?>" class="form-control">
									 	<?php
									}elseif ($data['tanggalSelesai'] != '0000-00-00') {
										echo date("d-m-Y", strtotime($data['tanggalSelesai']));
									} 
									?>
								</td>
								<td><?php echo $data['petugas']; ?></td>
								<td><button type="button" class="btn btn-info"><?php echo $data['status']; ?></button></td>
								<td><input type="submit" value="Proses" class="btn btn-success" name="selesai"></td>
							</tr>
							</form>
							<?php
						}
						?>
						</tbody>
					</table>
					</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="Selesai">
					<div class="panel panel-success">
						<div class="panel-heading">Daftar Selesai Perbaikan</div>
						<div class="panel-body">
			
					<table class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nama Pengajuan</th>
								<th>Keterangan</th>
								<th>Tgl Pengajuan</th>
								<th>Tgl Proses</th>
								<th>Tgl Selesai</th>
								<th>Petugas</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$no = 1;
						$sql = mysql_query("SELECT * FROM pengaduan WHERE status = 'Selesai Diperbaiki'");
						while ($data = mysql_fetch_array($sql)) {
							?>
							<tr>
								<td><?php echo $no++; ?></td>
								<td><?php echo $data['namaPengajuan']; ?></td>
								<td><?php echo $data['keterangan']; ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalPengajuan'])); ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalProses'])); ?></td>
								<td><?php echo date("d-m-Y", strtotime($data['tanggalSelesai'])); ?></td>
								<td><?php echo $data['petugas']; ?></td>
								<td><button type="button" class="btn btn-success"><?php echo $data['status']; ?></button></td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
					</div>
					</div>
				</div>
			</div>
        </div>
	<?php
    }
?>



<?php

// Check If form submitted, insert form data into users table.
if(isset($_POST['simpan'])) {

	$namaPengajuan = $_POST['namaPengajuan'];
	$tanggalPengajuan = date("Y-m-d", strtotime($_POST['tanggalPengajuan']));
	$keterangan = $_POST['keterangan'];

			
	// Insert user data into table
	$result2 = mysql_query("INSERT INTO pengaduan (namaPengajuan, keterangan, tanggalPengajuan, status) VALUES('$namaPengajuan','$keterangan','$tanggalPengajuan','Menunggu Antrian')");
	if ($result2) {
		echo '<meta http-equiv="refresh" content="0">';
	}
}

if (isset($_POST['proses'])) {

	$id = $_POST['id'];
	$petugas = $_POST['petugas'];
	$tanggalProses = date("Y-m-d", strtotime($_POST['tanggalProses']));

	$result = mysql_query("UPDATE pengaduan SET tanggalProses = '$tanggalProses', status = 'Proses Perbaikan', petugas = '$petugas' WHERE id = '$id'");

	if ($result) {
		echo '<meta http-equiv="refresh" content="0">';
	}

}

if (isset($_POST['selesai'])) {

	$id = $_POST['id'];
	$petugas = $_POST['petugas'];
	$tanggalSelesai = date("Y-m-d", strtotime($_POST['tanggalSelesai']));

	$result = mysql_query("UPDATE pengaduan SET tanggalSelesai = '$tanggalSelesai', status = 'Selesai Diperbaiki', petugas = '$petugas' WHERE id = '$id'");

	if ($result) {
		echo '<meta http-equiv="refresh" content="0">';
	}

}
?>
